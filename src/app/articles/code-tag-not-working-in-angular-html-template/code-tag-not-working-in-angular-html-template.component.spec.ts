import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeTagNotWorkingInAngularHtmlTemplateComponent } from './code-tag-not-working-in-angular-html-template.component';

describe('CodeTagNotWorkingInAngularHtmlTemplateComponent', () => {
  let component: CodeTagNotWorkingInAngularHtmlTemplateComponent;
  let fixture: ComponentFixture<CodeTagNotWorkingInAngularHtmlTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodeTagNotWorkingInAngularHtmlTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeTagNotWorkingInAngularHtmlTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
