import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { HomeComponent } from './home/home.component'; 
import { AboutComponent } from './about/about.component';  
import { ContactComponent } from './contact/contact.component';    
import { PortfolioComponent } from './portfolio/portfolio.component';  
import { SkillsComponent } from './skills/skills.component'; 
import { ResumeComponent } from './resume/resume.component'; 
import { ArticlesComponent } from './articles/articles.component'; 
import { CodeTagNotWorkingInAngularHtmlTemplateComponent } from './articles/code-tag-not-working-in-angular-html-template/code-tag-not-working-in-angular-html-template.component'; 
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

//This is my case 
const routes: Routes = [
  { path: '', redirectTo: '/about', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'portfolio', component: PortfolioComponent },
  { path: 'skills', component: SkillsComponent },
  { path: 'articles', component: ArticlesComponent },
  { path: 'resume', component: ResumeComponent },
  { path: 'articles/code-tag-not-working-in-angular-html-template', component: CodeTagNotWorkingInAngularHtmlTemplateComponent },
  { path: '**', component: PagenotfoundComponent },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
