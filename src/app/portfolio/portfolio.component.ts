import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  constructor() { }

  ngOnInit() {

   $(window).mousemove(function(e) {     
      $(".cursor").css({
        left: e.pageX,
        top: e.pageY
      })    
    })
    $(".cursor-link")
      .on("mouseenter", function() {   
      $('.cursor').addClass("active")   
    })
    .on("mouseleave", function() {    
      $('.cursor').removeClass("active")    
    })

  }

}
