import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
/* import { ParticlesModule } from 'angular-particle'; */
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { SkillsComponent } from './skills/skills.component';
import { ArticlesComponent } from './articles/articles.component';
import { CodeTagNotWorkingInAngularHtmlTemplateComponent } from './articles/code-tag-not-working-in-angular-html-template/code-tag-not-working-in-angular-html-template.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { ResumeComponent } from './resume/resume.component';




@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    ContactComponent,
    PortfolioComponent,
    HomeComponent,
    HeaderComponent,
    SkillsComponent,
    ArticlesComponent,
    CodeTagNotWorkingInAngularHtmlTemplateComponent,
    PagenotfoundComponent,
    ResumeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
   
    /* ParticlesModule */

  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
