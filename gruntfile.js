
module.exports = function (grunt) {
  grunt.initConfig({
    image: {
      static: {
        options: {
          optipng: false,
          pngquant: true,
          zopflipng: true,
          jpegRecompress: false,
          mozjpeg: true,
          guetzli: false,
          gifsicle: true,
          svgo: true
        }
      },
      dynamic: {
        files: [{
          expand: true,
          cwd: 'src/assets/images/',
          src: ['**/*.{png,jpg,gif,svg}'],
          dest: 'src/assets/images/compressed/'
        }]
      }
    }
  });
 
  grunt.loadNpmTasks('grunt-image');
};